select date(date) as date ,sum(prod_price * prod_qty) as ventes
	from transaction  
    where date between ’01-01-2019’ and ’31-12-2019’
	group by 1
	order by date ;

-- date(date) permet de ramener les dates en jour, car dans certain cas les heures et les minutes apparaissent
-- between construit un intervalle du 01-01-2019 inclus au 31-12-2019 inclus 
-- group by 1 permet de grouper suivant le premier élément,un raccourci cependant, cette façon de faire ne marche pas sur spark.

select client_id, 
	sum(case when product_type = ‘MEUBLE’ then prod_price * prod_qty else 0) as ventes_meuble,
	sum(case when product_type = ‘DECO’ then prod_price * prod_qty  else 0 ) as ventes_deco
	from transaction tr 
		join product_nomenclature pm
			on tr.prod_id = pm.product_id
	where date between ’01-01-2020’ and ’31-12-2020’
	group by 1 ;


-- sum(case …) va sommer les éléments qui remplissent une condition(when) sinon il va sommer 0, donc rien 
-- Id produit étant unique(d’après l’énoncé)  on peut faire la jointure sur ce champs